﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSonExample
{
  class Program
  {
    static void Main(string[] args)
    {
            /*
            KeyValuePair<string, string> myPair = new KeyValuePair<string, string>("asdfsadfa","dasfasdf");
            KeyValuePair<string, string> myPair2 = new KeyValuePair<string, string>("zzz", "bbb");
            KeyValuePair<string, string> myPair3 = new KeyValuePair<string, string>("ASD", "WER");
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            list.Add(myPair);
            list.Add(myPair2);
            list.Add(myPair3);
            string strResultJson = JsonConvert.SerializeObject(list);
            File.WriteAllText(@"blah.json", strResultJson);
            Console.WriteLine("Stored!");
            */

            /*
            Student student = new Student()
            {
                Id = 1,
                Name = "Wojtas",
                Degree = "MCA",
                Hobbies = new List<string>()
                {
                    "Reading",
                    "Playing Games"
                }
            };
            */

            
            string strResultJson = String.Empty;
            strResultJson = File.ReadAllText(@"blah.json");
            //JsonConvert.DeserializeObject()
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(strResultJson);
            foreach (var kv in dict)
            {
                Console.WriteLine(kv.Key + ":" + kv.Value);
            }
            Console.WriteLine(dict);
            

            /*
            string strResultJson = JsonConvert.SerializeObject(student);
            //Console.WriteLine(strResultJson);
            File.WriteAllText(@"student.json", strResultJson);
            Console.WriteLine("Stored!");

            strResultJson = String.Empty;
            strResultJson = File.ReadAllText(@"student.json");
            var resultStudent = JsonConvert.DeserializeObject<Student>(strResultJson);
            resultStudent.Degree = "Maths";
            Console.WriteLine(resultStudent.ToString());
            */

        }
  }
}
