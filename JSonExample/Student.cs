﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSonExample
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Degree { get; set; }
        public List<string> Hobbies { get; set; }

        public override string ToString()
        {
            return $"Student Information:\nId = {Id}, Name = {Name}, Degree = {Degree}, Hobbies = {string.Join(", ", Hobbies.ToArray())}";
        }
    }
}
//this is some comment
//second comment